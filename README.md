# AS2 & XML based Image Zoom Pan Control
![](deploy/zoom-pan-control.gif)

## XML Example
```xml
<config
	src="1.jpg"
	contentWidth="590"
	contentHeight="300"
	maxZoom="100"
	controlBoxAlign="right"
	controlBoxVerticalAlign="down"
	controlBoxMargin="10"
	autoHideTime="0"
/>
```
## License
MIT